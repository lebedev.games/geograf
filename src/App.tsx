import React, {Component, ReactNode} from 'react';
import {ipcRenderer} from 'electron';
import './App.css';

import {SceneEditor} from "./components/SceneEditor";
import {emptyScene} from "./domain/context";
import {Scene} from "./domain/model";
import {BehaviorSubject} from "rxjs";
import * as path from "path";



class App extends Component {

    updateSourceHandler?: (source: string) => void;
    updateSceneFileHandler?: (scene: Scene) => void;
    scene = new BehaviorSubject<Scene>(emptyScene);

    constructor(props: any) {
        super(props);
        this.state = {
            scene: emptyScene,
            sceneFile: null
        };
    }

    componentDidMount(): void {
        ipcRenderer.on('open-scene-file', (event, sceneFile: string, payload: any) => {
            let {scene, argv} = payload;
            let sceneName = path.basename(sceneFile).split('.')[0];
            document.title = `${sceneName} - GeoGraf v1.2.1`;
            this.scene.next(scene);
        });
        this.updateSceneFileHandler = (scene: Scene) => {
            ipcRenderer.send('update-scene-file', scene);
        };
        this.updateSourceHandler = (source: string) => {
            ipcRenderer.send('copy-source', source);
        };
        ipcRenderer.send('open-scene-file');
    }

    render(): ReactNode {
        return <SceneEditor
            observeTargetScene={() => this.scene.asObservable()}
            exportSource={source => this.updateSourceHandler ? this.updateSourceHandler(source): {}}
            modifyTargetScene={scene => this.updateSceneFileHandler ? this.updateSceneFileHandler(scene) : {}}
            default={emptyScene}
        />
    }

}

export default App;
