import {FeatureCollection} from "geojson";

export type Scene = FeatureCollection;

