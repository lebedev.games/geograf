import React, {Component, ReactNode} from "react";
import {Feature, GeoJsonProperties} from "geojson";
import {Observable, Subscription} from "rxjs";
import {Scene} from "../domain/model";
import * as L from "leaflet";
import {LatLng, LatLngTuple, Layer} from "leaflet";
import "leaflet-draw";
import "leaflet/dist/leaflet.css";
import "leaflet-draw/dist/leaflet.draw.css";
import './FeatureMap.scss';
import {v4 as uuid4} from "uuid";
import {StickyCircleMarker} from './extensions/leaflet/StickyCircleMarker';
import {StickyPolyline} from './extensions/leaflet/StickyPolyline';


export type FeatureType = 'rect' | 'polygon' | 'stickyPoint' | 'point' | 'line' | 'stickyLine';

export interface FeatureCreationForm {
    type: FeatureType,
    properties: GeoJsonProperties
}

interface FeatureMapProps {
    default: Scene
    zoomRequest: () => Observable<number>;
    toggleFeatureVisibleRequest: () => Observable<Feature>;
    lookAtFeatureRequest: () => Observable<Feature>;
    recreateSceneRequest: () => Observable<Scene>;
    createFeatureRequest: () => Observable<FeatureCreationForm>;
    updateFeatureRequest: () => Observable<Feature>;
    removeFeatureRequest: () => Observable<Feature>;
    defineFeatureRequest: () => Observable<Feature>;
    selectFeature: (feature: Feature | null) => void;
    createFeature: (feature: Feature) => void;
    updateFeature: (feature: Feature) => void;
    removeFeature: (feature: Feature) => void;
    changeFeatureMode: (mode: FeatureType | null) => void;
}

interface FeatureMapState {
    form?: FeatureCreationForm
}

export class FeatureMap extends Component<FeatureMapProps, FeatureMapState> {
    subscription = new Subscription();
    mapRef = React.createRef<HTMLDivElement>();

    hiddenFeaturesGroup = new L.GeoJSON();
    createdFeaturesGroup = new L.GeoJSON();
    editingFeaturesGroup = new L.FeatureGroup();
    editHandler: any = null;
    map: L.Map | null = null;

    control: any = {};

    componentDidMount(): void {
        this.configureMap();
        this.subscription.add(this.props.zoomRequest().subscribe(zoom => this.zoom(zoom)));
        this.subscription.add(this.props.toggleFeatureVisibleRequest().subscribe(feature => this.toggleFeatureVisible(feature)))
        this.subscription.add(this.props.lookAtFeatureRequest().subscribe(feature => this.lookAtFeature(feature)));
        this.subscription.add(this.props.recreateSceneRequest().subscribe(scene => this.recreateScene(scene)));
        this.subscription.add(this.props.createFeatureRequest().subscribe(form => this.startFeatureToCreate(form)));
        this.subscription.add(this.props.defineFeatureRequest().subscribe(feature => this.defineFeature(feature)));
        this.subscription.add(this.props.updateFeatureRequest().subscribe(feature => this.checkoutFeatureToEdit(feature)));
        this.subscription.add(this.props.removeFeatureRequest().subscribe(feature => this.removeFeature(feature)));
        document.addEventListener('keydown', this.onKeyDown);
    }

    componentWillUnmount(): void {
        this.subscription.unsubscribe();
        document.removeEventListener('keydown', this.onKeyDown)
    }

    onKeyDown = (e: KeyboardEvent) => {
        if (e.key === 'Escape') {
            this.commitEditingFeatures();
            this.cancelFeatureCreation();
        }
    }

    zoom(value: number): void {
        this.map?.zoomIn(value);
    }

    lookAtFeature(feature: Feature): void {
        for (let layerKey of Object.keys((this.map as any)._layers)) {
            let layer = (this.map as any)._layers[layerKey];

            if (layer.feature?.id === feature.id) {
                if (layer.feature.geometry.type === 'Point') {
                    let center = new LatLng(
                        layer.feature.geometry.coordinates[1],
                        layer.feature.geometry.coordinates[0],
                    );
                    this.map?.setView(center, this.map?.getZoom(), {animate: true});
                } else {

                    let bounds = layer.getBounds();
                    let center = bounds.getCenter();
                    this.map?.setView(center, this.map?.getZoom(), {animate: true});
                }
            }
        }
    }

    configureMap(): void {
        let projectCoordinates: LatLngTuple = [65.784240, 87.963515];
        let map = L.map(this.mapRef.current as HTMLElement, {
            drawControlTooltips: false,
            zoomControl: false,
            keyboard: false
        }).setView(projectCoordinates, 10);
        this.map = map;

        let tileLayerProvider = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}';
        let tileLayerOptions = {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/light-v10',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiZWxpYWdhbWVzIiwiYSI6ImNrOXplM3NybjBkcGMzZG52bnY1aXNkaTAifQ.k51q0OdlSKlor6Up948WuA'
        };
        L.tileLayer(tileLayerProvider, tileLayerOptions).addTo(map);
        L.control.scale({position: "bottomright", metric: true, imperial: false}).addTo(map);

        // map.addLayer(this.hiddenFeaturesGroup);

        let createdFeaturesGroup = this.createdFeaturesGroup;

        createdFeaturesGroup.options = {
            pointToLayer: (point, latlng) => {
                return L.circleMarker(latlng, {radius: 4})
            },
            style: feature => {
                if (feature?.geometry.type === 'LineString') {
                    return {
                        color: feature?.properties.color,
                        opacity: feature?.properties.opacity,
                        weight: 2.0,
                        fill: false,
                    }
                } else if (feature?.geometry.type === 'Point') {
                    return {
                        color: feature?.properties.color,
                        opacity: feature?.properties.opacity,
                        weight: 1.0,
                        fill: true,
                        fillColor: feature?.properties.fillColor,
                        fillOpacity: 1.0
                    }
                } else {
                    return {
                        color: feature?.properties.color,
                        opacity: feature?.properties.opacity,
                        weight: 1.0,
                        fill: true,
                        fillColor: feature?.properties.fillColor,
                        fillOpacity: feature?.properties.fillOpacity
                    }
                }


            }
        };
        map.addLayer(createdFeaturesGroup);



        let editingFeaturesGroup = this.editingFeaturesGroup;
        map.addLayer(editingFeaturesGroup);


        this.control['polygon'] = new L.Draw.Polygon(map, {
            allowIntersection: false, // Restricts shapes to simple polygons
            drawError: {
                color: '#e1e100', // Color the shape will turn when intersects
                message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
            },
            shapeOptions: {
                color: '#5F9BFA'
            }
        });


        this.control['rect'] = new L.Draw.Rectangle(map, {
            shapeOptions: {
                color: '#5F9BFA'
            },
        });

        this.control['point'] = new L.Draw.CircleMarker(map, {});
        this.control['point'].setOptions({radius: 4})

        this.control['stickyPoint'] = new (StickyCircleMarker as any)(map, {});

        this.control['line'] = new L.Draw.Polyline(map, {
            shapeOptions: {
                color: '#5F9BFA',
                weight: 10
            }
        });

        this.control['stickyLine'] = new (StickyPolyline as any)(map, {});

        // let drawControl = new L.Control.Draw(options as any);
        // map.addControl(drawControl);
        this.editHandler = new L.EditToolbar.Edit(map, {
            featureGroup: editingFeaturesGroup,
        } as any);

        let handleCreation = (event: L.DrawEvents.Created) => {
            let type = event.layerType;
            let layer = event.layer;

            if (type === 'polygon' || type === 'rectangle') {
                let polygon = layer.toGeoJSON();
                polygon.id = uuid4();
                polygon.properties.name = 'Polygon';
                polygon.properties = {
                    name: 'Polygon',
                    ...this.state.form?.properties
                };
                this.createFeature(polygon);

            } else if (type === 'polyline' || type === 'stickypolyline') {
                let lineString = layer.toGeoJSON();
                lineString.id = uuid4();
                lineString.properties = {
                    name: 'LineString',
                    ...this.state.form?.properties
                };
                this.createFeature(lineString);

            } else if (type === 'circlemarker' || type === 'stickycirclemarker') {
                let point = layer.toGeoJSON();
                point.id = uuid4();
                point.properties = {
                    name: 'Point',
                    ...this.state.form?.properties
                };
                this.createFeature(point);

            } else {
                throw Error('Unable to handle geometry creation, unknown type: ' + type);
            }
        }

        map.on('mousedown', event => {
            if ((event as any).originalEvent.button === 2) {
                this.cancelFeatureCreation();
            }
        });
        map.on(L.Draw.Event.DRAWSTART as any, () => this.commitEditingFeatures());
        map.on(L.Draw.Event.CREATED as any, handleCreation as any);
        map.on(L.Draw.Event.EDITED as any, () => this.commitEditingFeatures());
        editingFeaturesGroup.on('click', () => this.commitEditingFeatures());
        createdFeaturesGroup.on('click', event => this.checkoutFeatureToEdit(event.propagatedFrom.toGeoJSON()));
    }

    startFeatureToCreate(form: FeatureCreationForm): void {
        for (let type of Object.keys(this.control)) {
            let control = this.control[type];
            if (control.enabled()) {
                control.disable();
            }
        }

        let control = this.control[form.type];
        control.setOptions({
            repeatMode: true,
            shapeOptions: {
                color: form.properties?.color || '#5F9BFA'
            },
            color: form.properties?.color || '#5F9BFA',
        });
        control.enable();
        this.setState({form})
        this.props.changeFeatureMode(form.type);
    }

    cancelFeatureCreation() {
        for (let type of Object.keys(this.control)) {
            let control = this.control[type];
            if (control.enabled()) {
                control.disable();
            }
        }
        this.setState({form: undefined})
        this.props.changeFeatureMode(null);
    }

    recreateScene(scene: Scene): void {
        this.cancelFeatureCreation();
        this.editHandler.disable();
        this.props.selectFeature(null);
        const layers = this.getLayers();
        for (let layer of layers) {
            this.props.removeFeature((layer as any).toGeoJSON());
        }
        this.createdFeaturesGroup.clearLayers();
        this.hiddenFeaturesGroup.clearLayers();
        this.editingFeaturesGroup.clearLayers();

        for (let feature of scene.features) {
            this.createFeature(feature);
        }
    }

    commitEditingFeatures(): void {
        let layers: any[] = this.editingFeaturesGroup.getLayers();
        this.editingFeaturesGroup.clearLayers();
        for (let layer of layers) {
            let feature = layer.toGeoJSON();
            this.createdFeaturesGroup.addData(feature);
            this.updateFeature(feature);
        }
        this.editHandler.disable();
        this.props.selectFeature(null);
    }

    checkoutFeatureToEdit(target: Feature): void {
        this.cancelFeatureCreation();
        this.commitEditingFeatures();
        if (!target.properties?.hidden) {

            let layers = this.createdFeaturesGroup.getLayers();
            let index = layers.findIndex(layer => (layer as any).feature.id === target.id);
            if (index === -1) {
                throw Error(`Unable to checkout feature id=${target.id}, not found in created features group`);
            }
            let layer = layers[index];
            this.createdFeaturesGroup.removeLayer(layer);
            this.editingFeaturesGroup.addLayer(layer);
            this.editHandler.enable();
        }
        this.props.selectFeature(target);
    }

    createFeature(feature: Feature): void {
        try
        {
            if (feature.properties?.hidden) {
                this.hiddenFeaturesGroup.addData(feature);
            } else {
                this.createdFeaturesGroup.addData(feature);
            }
            this.props.createFeature(feature);
        }
        catch (error) {
            // TODO: show errors
            console.error(error, error.message);
        }
    }

    updateFeature(feature: Feature): void {
        this.props.updateFeature(feature);
    }

    defineFeature(target: Feature): void {
        let layers = this.getLayers();
        for (let layer of layers) {
            let featuredLayer = layer as any;
            if (featuredLayer.feature.id === target.id) {
                featuredLayer.feature.properties = target.properties;
                this.updateFeature(featuredLayer.feature);
            }
        }
    }

    toggleFeatureVisible(feature: Feature): void {
        this.cancelFeatureCreation();
        this.commitEditingFeatures();
        this.editHandler.disable();

        this.props.selectFeature(feature);

        let hidden = feature.properties?.hidden || false;
        hidden = !hidden;

        if (hidden) {
            let layers = this.createdFeaturesGroup.getLayers();
            for (let layer of layers) {
                if ((layer as any).feature.id === feature.id) {
                    this.createdFeaturesGroup.removeLayer(layer);
                }
            }
            (feature.properties as any).hidden = true;
            this.hiddenFeaturesGroup.addData(feature);
            this.updateFeature(feature);
        } else {
            let layers = this.hiddenFeaturesGroup.getLayers();
            for (let layer of layers) {
                if ((layer as any).feature.id === feature.id) {
                    this.hiddenFeaturesGroup.removeLayer(layer);
                }
            }
            (feature.properties as any).hidden = false;
            this.createdFeaturesGroup.addData(feature);
            this.updateFeature(feature);
        }
    }

    removeFeature(feature: Feature): void {
        this.cancelFeatureCreation();
        this.editHandler.disable();
        let layers = this.editingFeaturesGroup.getLayers();
        for (let layer of layers) {
            if ((layer as any).feature.id === feature.id) {
                this.editingFeaturesGroup.removeLayer(layer);
            }
        }
        layers = this.createdFeaturesGroup.getLayers();
        for (let layer of layers) {
            if ((layer as any).feature.id === feature.id) {
                this.createdFeaturesGroup.removeLayer(layer);
            }
        }
        layers = this.hiddenFeaturesGroup.getLayers();
        for (let layer of layers) {
            if ((layer as any).feature.id === feature.id) {
                this.hiddenFeaturesGroup.removeLayer(layer);
            }
        }
        this.props.selectFeature(null);
        this.props.removeFeature(feature);
    }

    private getLayers(): Layer[] {
        return this.createdFeaturesGroup.getLayers()
            .concat(this.editingFeaturesGroup.getLayers())
            .concat(this.hiddenFeaturesGroup.getLayers());
    }

    render(): ReactNode {
        return <div ref={this.mapRef} className="feature-map"/>
    }
}