import React, {Component, ReactNode} from "react";
import {Observable, Subject, Subscription} from "rxjs";
import {Scene} from "../domain/model";
import {FeatureCreationForm, FeatureMap, FeatureType} from "./FeatureMap";
import {Feature} from "geojson";
import {Icon} from "./Icon";
import CodeMirror from "codemirror";
import 'codemirror/mode/javascript/javascript';
import 'codemirror/theme/blackboard.css';
import 'codemirror/lib/codemirror.css';
import "./SceneEditor.scss";

interface FeatureColor {
    color: string;
    opacity: number;
    fillColor: string;
    fillOpacity: number;
}

interface SceneEditorState {
    scene: Scene;
    current: Feature | null;
    currentNameInputActive: boolean
    panel: SceneEditorPanel;
    mode: FeatureType | null;
    currentColor: number,
    palette: FeatureColor[],
}

interface SceneEditorProps {
    observeTargetScene: () => Observable<Scene>;
    exportSource: (source: string) => void;
    modifyTargetScene: (scene: Scene) => void;
    default: Scene;
}

type SceneEditorPanel = 'scene' | 'source' | 'console';


export class SceneEditor extends Component<SceneEditorProps, SceneEditorState> {
    subscription = new Subscription();

    zoomRequest = new Subject<number>();
    toggleFeatureVisibleRequest = new Subject<Feature>();
    updateFeatureRequest = new Subject<Feature>();
    recreateSceneRequest = new Subject<Scene>();
    lookAtFeatureRequest = new Subject<Feature>();
    removeFeatureRequest = new Subject<Feature>();
    defineFeatureRequest = new Subject<Feature>();
    createFeatureRequest = new Subject<FeatureCreationForm>();

    codeRef = React.createRef<HTMLTextAreaElement>();
    codeEditor: CodeMirror.EditorFromTextArea | null = null;

    constructor(props: SceneEditorProps) {
        super(props);
        this.state = {
            scene: props.default,
            current: null,
            currentNameInputActive: false,
            panel: 'scene',
            mode: null,
            currentColor: 0,
            palette: [
                {
                    color: '#5F9BFA',
                    opacity: 1.0,
                    fillColor: '#5F9BFA',
                    fillOpacity: 0.2,
                },
                {
                    color: '#F15050',
                    opacity: 1.0,
                    fillColor: '#F15050',
                    fillOpacity: 0.2
                },
                {
                    color: '#34AF7B',
                    opacity: 1.0,
                    fillColor: '#34AF7B',
                    fillOpacity: 0.2
                },
                {
                    color: '#1E7D7F',
                    opacity: 1.0,
                    fillColor: '#1E7D7F',
                    fillOpacity: 0.2
                },
                {
                    color: '#8495AC',
                    opacity: 0.7,
                    fillColor: '#8495AC',
                    fillOpacity: 0.1
                }
            ]
        };
    }

    componentDidMount(): void {
        this.subscription.add(this.props.observeTargetScene().subscribe(scene => this.recreateSceneRequest.next(scene)));

        this.codeEditor = CodeMirror.fromTextArea(this.codeRef.current as HTMLTextAreaElement, {
            lineNumbers: true,
            mode: 'javascript',
            readOnly: true
        });

        document.addEventListener('keydown', this.onKeyDown);
    }

    componentDidUpdate(): void {
        let source = JSON.stringify(this.state.scene, null, 2);
        this.codeEditor?.setValue(source);
    }

    componentWillUnmount(): void {
        this.subscription.unsubscribe();
        document.removeEventListener('keydown', this.onKeyDown);
    }

    onKeyDown = (e: KeyboardEvent) => {
        if (e.key === 'Tab') {
            if (this.state.current && this.state.scene.features.length > 1) {
                let index = this.state.scene.features.indexOf(this.state.current);
                let direction = e.shiftKey ? -1 : 1;
                index = (index + direction) % this.state.scene.features.length;
                if (index === -1) {
                    index = this.state.scene.features.length - 1;
                }
                let feature = this.state.scene.features[index];
                this.lookAtFeatureRequest.next(feature);
                this.updateFeatureRequest.next(feature);
                e.stopPropagation();
                e.preventDefault();
            }
        }

        if (e.key === 'ArrowDown') {
            if (this.state.current) {
                let index = this.state.scene.features.indexOf(this.state.current);
                if (index + 1 < this.state.scene.features.length) {
                    let feature = this.state.scene.features[index + 1];
                    this.updateFeatureRequest.next(feature);
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
        }

        if (e.key === 'ArrowUp') {
            if (this.state.current) {
                let index = this.state.scene.features.indexOf(this.state.current);
                if (index - 1 >= 0) {
                    let feature = this.state.scene.features[index - 1];
                    this.updateFeatureRequest.next(feature);
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
        }
    }

    openPanel(panel: SceneEditorPanel) {
        this.setState({panel});
    }

    changeFeatureName(feature: Feature, name: string) {
        if (feature.properties) {
            feature.properties.name = name;
        } else {
            feature.properties = {name}
        }
        this.defineFeatureRequest.next(feature);
    }

    render(): ReactNode {
        let features = [];

        let featureIcon = {
            'Point': 'point',
            'MultiPoint': 'point',
            'LineString': 'line',
            'MultiLineString': 'line',
            'Polygon': 'polygon',
            'MultiPolygon': 'polygon',
            'GeometryCollection': 'polygon'
        };

        let properties = [];

        for (let feature of this.state.scene.features) {

            let icon = featureIcon[feature.geometry.type];
            let name = feature.properties?.name || '';
            let color = feature.properties?.color || 'white';
            let hidden = feature.properties?.hidden || false;
            let hiddenIcon = hidden ? 'hidden' : 'shown';

            let component;
            if (this.state.current && this.state.current.id === feature.id) {
                let objectProperties = this.state.current?.properties || {};
                for (let key of Object.keys(objectProperties)) {
                    let value = objectProperties[key];
                    if (typeof value === 'string') {
                        value = '"' + value + '"';
                    } else if (typeof value === 'number') {

                    } else if (typeof value === 'bigint') {
                    } else if (typeof value === 'boolean') {
                        value = JSON.stringify(value);
                    } else {
                        value = 'object';
                    }
                    let property = (
                        <div key={key} className="property">
                            <div className="name">{key}:</div>
                            <div className="value">{value}</div>
                        </div>
                    );

                    properties.push(property);
                }

                component = (
                    <div key={feature.id} className="feature active">
                        <Icon name={icon} width={16} height={16} color={color}/>
                        <input className="feature-name-editor"
                               autoFocus={true}
                               onFocus={() => this.setState({currentNameInputActive: true})}
                               onBlur={() => this.setState({currentNameInputActive: false})}
                               onChange={event => this.changeFeatureName(feature, event.target.value)} value={name}/>
                        {
                            this.state.currentNameInputActive && <div className="feature-delete">
                                <Icon name="edit" width={16} height={16} color="#77F4E8"/>
                            </div>
                        }
                        {
                            !this.state.currentNameInputActive && <div className="feature-delete">
                                <Icon name="edit" width={16} height={16} color="#98B5D6"/>
                            </div>
                        }
                        <div className="feature-delete" onClick={() => this.removeFeatureRequest.next(feature)}>
                            <Icon name="delete" width={16} height={16} color="#FD603C"/>
                        </div>
                        <div className="feature-delete" onClick={() => this.lookAtFeatureRequest.next(feature)}>
                            <Icon name="focus" width={16} height={16} color="#CFD8DD"/>
                        </div>
                        <div className="feature-delete" onClick={() => this.toggleFeatureVisibleRequest.next(feature)}>
                            <Icon name={hiddenIcon} width={16} height={16} color="#CFD8DD"/>
                        </div>
                    </div>
                );
            } else {
                component = (
                    <div key={feature.id} className="feature" onClick={() => this.updateFeatureRequest.next(feature)}>
                        <Icon name={icon} width={16} height={16} color={color}/>
                        <div className="feature-name">{name}</div>
                        {hidden &&
                        <div className="feature-delete" onClick={() => this.toggleFeatureVisibleRequest.next(feature)}>
                            <Icon name="hidden" width={16} height={16} color="#CFD8DD"/>
                        </div>}
                    </div>
                );
            }
            features.push(component);
        }

        let panelClassName = (route: string) => this.state.panel === route ? route + ' active' : route;
        let toolClassName = (type: FeatureType) => (this.state.mode === type) ? 'tool active' : 'tool';

        let sidePanelBodyClass = (panel: string) => this.state.panel === panel ? 'side-panel-body active' : 'side-panel-body';

        return (
            <main>
                <div className="side-panel">
                    <nav className="side-bar">
                        <div className={panelClassName("scene")} onClick={() => this.openPanel('scene')}>
                            <Icon name="scene" width={24} height={24}/>
                        </div>
                        <div className={panelClassName("source")} onClick={() => this.openPanel('source')}>
                            <Icon name="source" width={24} height={24}/>
                        </div>
                    </nav>
                    <div className={sidePanelBodyClass('scene')}>
                        <header>
                            Hierarchy
                        </header>
                        <section className="features-section">
                            {features}
                        </section>
                        {properties.length > 0 && <section className="properties-section">
                            <div className="properties">
                                {properties}
                            </div>
                        </section>
                        }
                    </div>
                    <div className={sidePanelBodyClass('source')}>
                        <header className="side-panel-header">
                            <span>GeoJSON</span>
                            <div className="icon"
                                 onClick={() => this.props.exportSource(JSON.stringify(this.state.scene, null, 2))}>
                                <Icon name="copy" width={16} height={16} color="#98B5D6"/>
                            </div>
                        </header>
                        <section className="code-section">
                            <textarea ref={this.codeRef}/>
                        </section>
                    </div>
                    <div className={sidePanelBodyClass('console')}>
                        <header className="side-panel-header">
                            <span>Console</span>
                        </header>
                        <section className="code-section">
                            Hellou
                        </section>
                    </div>
                </div>
                <div className="map">
                    <FeatureMap
                        default={this.props.default}
                        zoomRequest={() => this.zoomRequest.asObservable()}
                        toggleFeatureVisibleRequest={() => this.toggleFeatureVisibleRequest.asObservable()}
                        lookAtFeatureRequest={() => this.lookAtFeatureRequest.asObservable()}
                        createFeatureRequest={() => this.createFeatureRequest.asObservable()}
                        updateFeatureRequest={() => this.updateFeatureRequest.asObservable()}
                        defineFeatureRequest={() => this.defineFeatureRequest.asObservable()}
                        recreateSceneRequest={() => this.recreateSceneRequest.asObservable()}
                        removeFeatureRequest={() => this.removeFeatureRequest.asObservable()}
                        selectFeature={feature => this.selectFeature(feature)}
                        createFeature={feature => this.createFeature(feature)}
                        updateFeature={feature => this.updateFeature(feature)}
                        removeFeature={feature => this.removeFeature(feature)}
                        changeFeatureMode={mode => this.setState({mode})}
                    />
                </div>
                <div className="tools-bar">
                    <div className="colors-palette">
                        <div className="variant facility" onClick={() => this.changeColor(0)}>
                            <div className="variant-background"/>
                            <Icon name="rect" width={24} height={24}/>
                            {this.state.currentColor === 0 &&
                            <div className="variant-selection">
                                <Icon name="select" width={16} height={16}/>
                            </div>
                            }
                        </div>
                        <div className="variant restricted-area" onClick={() => this.changeColor(1)}>
                            <div className="variant-background"/>
                            <Icon name="rect" width={24} height={24}/>
                            {this.state.currentColor === 1 &&
                            <div className="variant-selection">
                                <Icon name="select" width={16} height={16}/>
                            </div>
                            }
                        </div>
                        <div className="variant area-a" onClick={() => this.changeColor(2)}>
                            <div className="variant-background"/>
                            <Icon name="rect" width={24} height={24}/>
                            {this.state.currentColor === 2 &&
                            <div className="variant-selection">
                                <Icon name="select" width={16} height={16}/>
                            </div>
                            }
                        </div>
                        <div className="variant area-b" onClick={() => this.changeColor(3)}>
                            <div className="variant-background"/>
                            <Icon name="rect" width={24} height={24}/>
                            {this.state.currentColor === 3 &&
                            <div className="variant-selection">
                                <Icon name="select" width={16} height={16}/>
                            </div>
                            }
                        </div>
                        <div className="variant bounding-area" onClick={() => this.changeColor(4)}>
                            <Icon name="transparent" width={24} height={24}/>
                            {this.state.currentColor === 4 &&
                            <div className="variant-selection">
                                <Icon name="select" width={16} height={16}/>
                            </div>
                            }
                        </div>
                    </div>
                    <div className="tools-panel">
                        <div className={toolClassName('line')} onClick={() => this.changeFeatureMode('line')}>
                            <Icon name="line" width={24} height={24}/>
                        </div>
                        <div className={toolClassName('stickyLine')}
                             onClick={() => this.changeFeatureMode('stickyLine')}>
                            <Icon name="stickyLine" width={24} height={24}/>
                        </div>
                        <div className={toolClassName('rect')} onClick={() => this.changeFeatureMode('rect')}>
                            <Icon name="rect" width={24} height={24}/>
                        </div>
                        <div className={toolClassName('polygon')} onClick={() => this.changeFeatureMode('polygon')}>
                            <Icon name="polygon" width={24} height={24}/>
                        </div>
                        <div className={toolClassName('point')} onClick={() => this.changeFeatureMode('point')}>
                            <Icon name="point" width={24} height={24}/>
                        </div>
                        <div className={toolClassName('stickyPoint')}
                             onClick={() => this.changeFeatureMode('stickyPoint')}>
                            <Icon name="stickyPoint" width={24} height={24}/>
                        </div>
                    </div>
                </div>
                <div className="zoom-panel">
                    <div className="zoom-button" onClick={() => this.zoomRequest.next(1)}>
                        <Icon name="plus" width={16} height={16}/>
                    </div>
                    <div className="zoom-separator"/>
                    <div className="zoom-button" onClick={() => this.zoomRequest.next(-1)}>
                        <Icon name="minus" width={16} height={16}/>
                    </div>
                </div>

            </main>
        );
    }

    selectFeature(feature: Feature | null) {
        this.setState({
            current: feature,
            currentNameInputActive: true
        });
    }

    createFeature(feature: Feature): void {
        let features = this.state.scene.features.concat(feature);
        let scene = {
            type: this.state.scene.type,
            features: features,
        };
        this.setState({scene});
        this.props.modifyTargetScene(scene);
    }

    updateFeature(target: Feature): void {
        let features = this.state.scene.features.map(feature => feature.id === target.id ? target : feature);
        let scene = {
            type: this.state.scene.type,
            features: features
        };
        this.setState({scene});
        this.props.modifyTargetScene(scene);
    }

    removeFeature(target: Feature): void {
        let features = this.state.scene.features.filter(feature => feature.id !== target.id);
        let scene = {
            type: this.state.scene.type,
            features: features
        };
        this.setState({scene});
        this.props.modifyTargetScene(scene);
    }

    changeFeatureMode(type: FeatureType) {
        this.createFeatureRequest.next({
            type: type,
            properties: {
                ...this.state.palette[this.state.currentColor],
                hidden: false
            },
        });
    }

    changeColor(currentColor: number) {
        this.setState({currentColor});
        if (this.state.mode) {
            this.createFeatureRequest.next({
                type: this.state.mode,
                properties: {
                    ...this.state.palette[currentColor],
                    hidden: false
                },
            });
        }
    }
}