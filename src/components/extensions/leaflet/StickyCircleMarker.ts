import * as L from "leaflet";
import {LatLng} from "leaflet";
import {Candidate, getDistance, getPointProjection} from "../math";

export const StickyCircleMarker = L.Draw.Marker.extend({
    statics: {
        TYPE: 'stickycirclemarker'
    },

    options: {
        radius: 4,
        stroke: true,
        color: '#3388ff',
        weight: 2,
        opacity: 0.5,
        fill: true,
        fillColor: null, //same as color by default
        fillOpacity: 0.2,
        clickable: true,
        zIndexOffset: 2000 // This should be > than the highest z-index any markers
    },

    // @method initialize(): void
    initialize: function (map: any, options: any) {
        // Save the type so super can fire, need to do this as cannot do this.TYPE :(
        this.type = 'stickycirclemarker';

        this._initialLabelText = L.drawLocal.draw.handlers.circlemarker.tooltip.start;

        L.Draw.Feature.prototype.initialize.call(this, map, options);
    },

    _onMouseMove: function (e: any) {
        var latlng = e.latlng;

        latlng = this._stickExistingGeometry(latlng);

        this._tooltip.updatePosition(latlng);
        this._mouseMarker.setLatLng(latlng);

        if (!this._marker) {
            this._marker = this._createMarker(latlng);
            // Bind to both marker and map to make sure we get the click event.
            this._marker.on('click', this._onClick, this);
            this._map
                .on('click', this._onClick, this)
                .addLayer(this._marker);
        } else {
            latlng = this._mouseMarker.getLatLng();
            this._marker.setLatLng(latlng);
        }
    },

    _stickExistingGeometry: function (markerLatLng: LatLng): LatLng {
        let point = {
            x: markerLatLng.lng,
            y: markerLatLng.lat
        };
        let candidates: Candidate[] = [];
        for (let layerId of Object.keys(this._map._layers)) {
            let layer = this._map._layers[layerId];
            if (layer.feature && layer.feature.id) {
                let coordinates = [];
                if (layer.feature.geometry.type ===  'Polygon') {
                    coordinates = layer.feature.geometry.coordinates[0];
                }
                if (layer.feature.geometry.type ===  'LineString') {
                    coordinates = layer.feature.geometry.coordinates;
                }

                for (let i = 0; i < coordinates.length - 1; i++) {
                    let start = {x: coordinates[i][0], y: coordinates[i][1]};
                    let end = {x: coordinates[i + 1][0], y: coordinates[i + 1][1]};

                    let projection = getPointProjection(start, end, point);
                    if (projection.onSegment) {
                        candidates.push({
                            point: projection.point,
                            id: layer.feature.id,
                            distance: getDistance(point, projection.point)
                        });
                    } else {
                        candidates.push({
                            point: start,
                            id: layer.feature.id,
                            distance: getDistance(point, start)
                        });
                        candidates.push({
                            point: end,
                            id: layer.feature.id,
                            distance: getDistance(point, end)
                        });
                    }
                }

            }
        }

        if (candidates.length > 0) {
            let closest = candidates.sort((a, b) => a.distance - b.distance)[0];
            markerLatLng = new LatLng(closest.point.y, closest.point.x);
        }

        return markerLatLng;
    },

    _fireCreatedEvent: function () {
        let markerLatLng = this._marker.getLatLng();
        var circleMarker = new L.CircleMarker(markerLatLng, this.options);
        (L.Draw.Feature.prototype as any)._fireCreatedEvent.call(this, circleMarker);
    },

    _createMarker: function (latlng: any) {

        this.options.color = this.options.shapeOptions?.color || this.options.color;
        return new L.CircleMarker(latlng, this.options);
    }
});