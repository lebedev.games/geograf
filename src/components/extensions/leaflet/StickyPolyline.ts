import * as L from "leaflet";
import {LatLng} from "leaflet";
import {Candidate, getDistance} from "../math";

export const StickyPolyline = L.Draw.Polyline.extend({
    statics: {
        TYPE: 'stickypolyline'
    },

    Poly: L.Polyline,

    options: {
        allowIntersection: true,
        repeatMode: false,
        drawError: {
            color: '#b00b00',
            timeout: 2500
        },
        icon: new L.DivIcon({
            iconSize: new L.Point(8, 8),
            className: 'leaflet-div-icon leaflet-editing-icon'
        }),
        touchIcon: new L.DivIcon({
            iconSize: new L.Point(20, 20),
            className: 'leaflet-div-icon leaflet-editing-icon leaflet-touch-icon'
        }),
        guidelineDistance: 20,
        maxGuideLineLength: 4000,
        shapeOptions: {
            stroke: true,
            color: '#3388ff',
            weight: 4,
            opacity: 0.5,
            fill: false,
            clickable: true
        },
        metric: true, // Whether to use the metric measurement system or imperial
        feet: true, // When not metric, to use feet instead of yards for display.
        nautic: false, // When not metric, not feet use nautic mile for display
        showLength: true, // Whether to display distance in the tooltip
        zIndexOffset: 2000, // This should be > than the highest z-index any map layers
        factor: 1, // To change distance calculation
        maxPoints: 0 // Once this number of points are placed, finish shape
    },

    // @method initialize(): void
    initialize: function (map: any, options: any) {
        // if touch, switch to touch icon
        if (L.Browser.touch) {
            this.options.icon = this.options.touchIcon;
        }

        // Need to set this here to ensure the correct message is used.
        this.options.drawError.message = L.drawLocal.draw.handlers.polyline.error;

        // Merge default drawError options with custom options
        if (options && options.drawError) {
            options.drawError = L.Util.extend({}, this.options.drawError, options.drawError);
        }

        // Save the type so super can fire, need to do this as cannot do this.TYPE :(
        this.type = 'stickypolyline';

        L.Draw.Feature.prototype.initialize.call(this, map, options);
    },


    // @method deleteLastVertex(): void
    // Remove the last vertex from the polyline, removes polyline from map if only one point exists.
    deleteLastVertex: function () {
        if (this._markers.length <= 1) {
            return;
        }

        var lastMarker = this._markers.pop(),
            poly = this._poly,
            // Replaces .spliceLatLngs()
            latlngs = poly.getLatLngs(),
            latlng = latlngs.splice(-1, 1)[0];
        this._poly.setLatLngs(latlngs);

        this._markerGroup.removeLayer(lastMarker);

        if (poly.getLatLngs().length < 2) {
            this._map.removeLayer(poly);
        }

        this._vertexChanged(latlng, false);
    },

    // @method addVertex(): void
    // Add a vertex to the end of the polyline
    addVertex: function (latlng: LatLng) {
        var markersLength = this._markers.length;
        // markersLength must be greater than or equal to 2 before intersections can occur
        if (markersLength >= 2 && !this.options.allowIntersection && this._poly.newLatLngIntersects(latlng)) {
            this._showErrorTooltip();
            return;
        } else if (this._errorShown) {
            this._hideErrorTooltip();
        }

        this._markers.push(this._createMarker(latlng));

        this._poly.addLatLng(latlng);

        if (this._poly.getLatLngs().length === 2) {
            this._map.addLayer(this._poly);
        }

        this._vertexChanged(latlng, true);
    },


    _onMouseMove: function (e: any) {
        var newPos = this._map.mouseEventToLayerPoint(e.originalEvent);
        var latlng = this._map.layerPointToLatLng(newPos);

        latlng = this._stickExistingPoint(latlng, newPos);


        // Save latlng
        // should this be moved to _updateGuide() ?
        this._currentLatLng = latlng;

        this._updateTooltip(latlng);

        // Update the guide line
        this._updateGuide(latlng);

        // Update the mouse marker position
        this._mouseMarker.setLatLng(latlng);

        L.DomEvent.preventDefault(e.originalEvent);
    },

    _onMouseDown: function (e: any) {
        if (!this._clickHandled && !this._touchHandled && !this._disableMarkers) {
            this._onMouseMove(e);
            this._clickHandled = true;
            this._disableNewMarkers();
            var originalEvent = e.originalEvent;
            var clientX = originalEvent.clientX;
            var clientY = originalEvent.clientY;
            this._startPoint.call(this, clientX, clientY);
        }
    },

    _startPoint: function (clientX: any, clientY: any) {

        this._mouseDownOrigin = L.point(clientX, clientY);
    },

    _onMouseUp: function (e: any) {
        var originalEvent = e.originalEvent;
        var clientX = originalEvent.clientX;
        var clientY = originalEvent.clientY;
        this._endPoint.call(this, clientX, clientY, e);
        this._clickHandled = null;
    },

    _endPoint: function (clientX: any, clientY: any, e: any) {

        if (this._mouseDownOrigin) {
            var dragCheckDistance = L.point(clientX, clientY)
                .distanceTo(this._mouseDownOrigin);


            let latlng = this._currentLatLng;


            var lastPtDistance = this._calculateFinishDistance(latlng);
            if (this.options.maxPoints > 1 && this.options.maxPoints === this._markers.length + 1) {
                this.addVertex(latlng);
                this._finishShape();
            } else if (lastPtDistance < 10 && L.Browser.touch) {
                this._finishShape();
            } else if (Math.abs(dragCheckDistance) < 9 * (window.devicePixelRatio || 1)) {
                this.addVertex(latlng);
            }
            this._enableNewMarkers(); // after a short pause, enable new markers
        }
        this._mouseDownOrigin = null;
    },

    _stickExistingPoint: function (markerLatLng: LatLng, originLayerPoint: any): LatLng {
        if (!originLayerPoint) {
            return markerLatLng;
        }
        let point = {
            x: markerLatLng.lng,
            y: markerLatLng.lat
        };
        let candidates: Candidate[] = [];
        for (let layerId of Object.keys(this._map._layers)) {
            let layer = this._map._layers[layerId];
            if (layer.feature && layer.feature.id && layer.feature.geometry.type === 'Point') {
                let coordinates = [layer.feature.geometry.coordinates];


                for (let i = 0; i < coordinates.length; i++) {
                    let start = {x: coordinates[i][0], y: coordinates[i][1]};
                    let layerPoint = this._map.latLngToLayerPoint({lat: start.y, lng: start.x});

                    candidates.push({
                        point: start,
                        id: layer.feature.id,
                        distance: getDistance(point, start)
                    });

                    if (getDistance(layerPoint, originLayerPoint) < 50 * 50) {
                    }
                }

            }
        }

        if (candidates.length > 0) {
            let closest = candidates.sort((a, b) => a.distance - b.distance)[0];
            markerLatLng = new LatLng(closest.point.y, closest.point.x);
        }

        return markerLatLng;
    },

    // ontouch prevented by clickHandled flag because some browsers fire both click/touch events,
    // causing unwanted behavior
    _onTouch: function (e: any) {
        var originalEvent = e.originalEvent;
        var clientX;
        var clientY;
        if (originalEvent.touches && originalEvent.touches[0] && !this._clickHandled && !this._touchHandled && !this._disableMarkers) {
            clientX = originalEvent.touches[0].clientX;
            clientY = originalEvent.touches[0].clientY;
            this._disableNewMarkers();
            this._touchHandled = true;
            this._startPoint.call(this, clientX, clientY);
            this._endPoint.call(this, clientX, clientY, e);
            this._touchHandled = null;
        }
        this._clickHandled = null;
    },

    _onMouseOut: function () {
        if (this._tooltip) {
            this._tooltip._onMouseOut.call(this._tooltip);
        }
    },

    _createMarker: function (latlng: LatLng) {
        var marker = new L.Marker(latlng, {
            icon: this.options.icon,
            zIndexOffset: this.options.zIndexOffset * 2
        });

        this._markerGroup.addLayer(marker);

        return marker;
    },

    _updateGuide: function (newPos?: LatLng) {
        var markerCount = this._markers ? this._markers.length : 0;

        if (markerCount > 0) {
            newPos = newPos || this._currentLatLng;

            // draw the guide line
            this._clearGuides();

            this._drawGuide(
                this._map.latLngToLayerPoint(this._markers[markerCount - 1].getLatLng()),
                this._map.latLngToLayerPoint(newPos)
            );
        }
    },

    _drawGuide: function (pointA: any, pointB: any) {
        var length = Math.floor(Math.sqrt(Math.pow((pointB.x - pointA.x), 2) + Math.pow((pointB.y - pointA.y), 2))),
            guidelineDistance = this.options.guidelineDistance,
            maxGuideLineLength = this.options.maxGuideLineLength,
            // Only draw a guideline with a max length
            i = length > maxGuideLineLength ? length - maxGuideLineLength : guidelineDistance,
            fraction,
            dashPoint,
            dash;

        //create the guides container if we haven't yet
        if (!this._guidesContainer) {
            this._guidesContainer = L.DomUtil.create('div', 'leaflet-draw-guides', this._overlayPane);
        }

        //draw a dash every GuildeLineDistance
        for (; i < length; i += this.options.guidelineDistance) {
            //work out fraction along line we are
            fraction = i / length;

            //calculate new x,y point
            dashPoint = {
                x: Math.floor((pointA.x * (1 - fraction)) + (fraction * pointB.x)),
                y: Math.floor((pointA.y * (1 - fraction)) + (fraction * pointB.y))
            };

            //add guide dash to guide container
            dash = L.DomUtil.create('div', 'leaflet-draw-guide-dash', this._guidesContainer);
            dash.style.backgroundColor =
                !this._errorShown ? this.options.shapeOptions.color : this.options.drawError.color;
            L.DomUtil.setPosition(dash, dashPoint as any);
        }
    },

    _updateGuideColor: function (color: any) {
        if (this._guidesContainer) {
            for (var i = 0, l = this._guidesContainer.childNodes.length; i < l; i++) {
                this._guidesContainer.childNodes[i].style.backgroundColor = color;
            }
        }
    },
});