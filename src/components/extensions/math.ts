export interface Point {
    x: number;
    y: number;
}

export interface Projection {
    point: Point,
    onSegment: boolean
}

export interface Candidate {
    point: Point,
    id: string,
    distance: number
}

export function getPointProjection(start: Point, end: Point, point: Point): Projection {
    let px = end.x - start.x, py = end.y - start.y;
    let dAB = px * px + py * py;
    let u = ((point.x - start.x) * px + (point.y - start.y) * py) / dAB;
    let x = start.x + u * px, y = start.y + u * py;
    return {
        point: {x, y},
        onSegment: (u >= 0.0 && u <= 1.0)
    };
}


export function getDistance(start: Point, end: Point): number {
    return (end.x - start.x) ** 2 + (end.y - start.y) ** 2;
}