const {app, BrowserWindow, ipcMain, dialog, Menu, clipboard} = require('electron');
const path = require('path')
const fs = require('fs');
const isDev = require('electron-is-dev');

entry = {
    sceneFile: null,
    mainWindow: null
}

function createWindow() {
    const isMac = process.platform === 'darwin';

    const filters = [
        {
            name: 'GeoGraf',
            extensions: ['geograf']
        }
    ]

    const template = [
        ...(isMac ? [{
            label: 'GeoGraf',
            role: 'appMenu',
            submenu: [
                {role: 'about'},
                {type: 'separator'},
                {role: 'services'},
                {type: 'separator'},
                {role: 'hide'},
                {role: 'hideothers'},
                {role: 'unhide'},
                {type: 'separator'},
                {role: 'quit'}
            ]
        }] : []),
        {
            label: 'File',
            role: 'fileMenu',
            submenu: [
                {
                    label: 'New', role: 'newFile', click: async () => {
                        let sceneFile = dialog.showSaveDialogSync({});
                        if (!sceneFile) {
                            return
                        }
                        let scene = {
                            "type": "FeatureCollection",
                            "features": []
                        }
                        fs.writeFileSync(sceneFile, JSON.stringify(scene, null, 2));
                        entry.sceneFile = sceneFile;
                    }
                },
                {
                    label: 'Open', role: 'openFile', click: () => {
                        let sceneFiles = dialog.showOpenDialogSync({filters, properties: ['openFile']});
                        if (sceneFiles.length === 0) {
                            return
                        }
                        entry.sceneFile = sceneFiles[0];
                        let scene = JSON.parse(fs.readFileSync(entry.sceneFile));
                        entry.mainWindow.webContents.send('open-scene-file', entry.sceneFile, {scene, argv: process.argv});
                    }
                },
                {
                    label: 'Save As...', role: 'saveAs', click: () => {
                        let sceneFile = dialog.showSaveDialogSync({});
                        if (!sceneFile) {
                            return
                        }
                        let scene = JSON.parse(fs.readFileSync(entry.sceneFile));
                        fs.writeFileSync(sceneFile, JSON.stringify(scene, null, 2));
                        entry.sceneFile = sceneFile;
                    }
                },
                isMac ? {role: 'close'} : {role: 'quit'}
            ]
        },
        {
            label: 'Edit',
            submenu: [
                { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
                { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
                { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
            ]
        },
        {
            label: 'Window',
            role: 'windowMenu',
            submenu: [
                {role: 'minimize'},
                {role: 'zoom'},
                ...(isMac ? [
                    {type: 'separator'},
                    {role: 'front'},
                    {type: 'separator'},
                    {role: 'window'}
                ] : [
                    {role: 'close'}
                ])
            ]
        },
    ]
    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu);

    let mainWindow;
    if (isDev) {
        mainWindow = new BrowserWindow({
            width: 1580,
            height: 900,
            webPreferences: {
                nodeIntegration: true,
                // preload: path.join(__dirname, 'preload.js')
            },
            icon: path.join(__dirname, 'assets/icon.png')
        });
        mainWindow.loadURL('http://localhost:3000');
        mainWindow.webContents.openDevTools();
    } else {
        mainWindow = new BrowserWindow({
            width: 1280,
            height: 900,
            webPreferences: {
                nodeIntegration: true,
            },
            icon: path.join(__dirname, 'assets/icon.png')
        });
        const index = path.join(__dirname, '../build/index.html');
        mainWindow.loadURL(`file://${index}`);
    }
    mainWindow.on('closed', () => entry.mainWindow = null);
    entry.mainWindow = mainWindow;

    if (!entry.sceneFile && process.argv.length > 1 && process.argv[1].includes('.geograf')) {
        entry.sceneFile = process.argv[1];
    }

    if (!entry.sceneFile) {
        let sceneFile = dialog.showSaveDialogSync(mainWindow, {});
        if (!sceneFile) {
            app.exit();
        }
        let scene = {
            "type": "FeatureCollection",
            "features": []
        }
        fs.writeFileSync(sceneFile, JSON.stringify(scene, null, 2));
        entry.sceneFile = sceneFile;
    }
}

app.on('open-file', (event, path) => {
    entry.sceneFile = path;
    event.preventDefault();
    if (entry.mainWindow) {
        let scene = JSON.parse(fs.readFileSync(entry.sceneFile));
        entry.mainWindow.webContents.send('open-scene-file', entry.sceneFile, {scene, argv: process.argv});
    }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
})

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
})


ipcMain.on('update-scene-file', (event, scene) => {
    fs.writeFileSync(entry.sceneFile, JSON.stringify(scene, null, 2));
})

ipcMain.on('open-scene-file', event => {
    let scene = JSON.parse(fs.readFileSync(entry.sceneFile));
    event.reply('open-scene-file', entry.sceneFile, {scene, argv: process.argv});
})

ipcMain.on('copy-source', (event, source) => {
    clipboard.writeText(source);
})

